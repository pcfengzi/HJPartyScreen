import Vue from 'vue'
import App from './App.vue'
import BaiduMap from 'vue-baidu-map'
// import layer from 'vue-layer'

import './assets/common.less'

import dataV from '@jiaminghi/data-view'
// 引用API文件
import api from './api/api.js'
// 将API方法绑定到全局
Vue.prototype.$api = api
// Vue.prototype.$layer = layer(Vue);

Vue.config.productionTip = false

Vue.use(dataV)

Vue.use(BaiduMap, {ak: 'oKMk9EFojKczNhR5QI7PWWaW'})
new Vue({
  render: h => h(App)
}).$mount('#app')
